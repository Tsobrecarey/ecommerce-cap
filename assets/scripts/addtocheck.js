let addtocheckbtn=document.querySelectorAll(".checkbtn");

addtocheckbtn.forEach(indiv_btn=>{
	indiv_btn.addEventListener("click", btn=>{
		let id=btn.target.getAttribute('data-id');
		let quantity=btn.target.previousElementSibling.value;
		
		if(quantity<=0){
			alert("Please enter valid quantity");
		}else{
			let data=new FormData;
			data.append("id", id);
			data.append("checkout", quantity);

			fetch("../../controllers/addtocheck-process.php",{
				method:"POST", body: data
			})
			.then(response=>{
				return response.text();
			})
			.then(res=>{
				document.getElementById("checkcount").textContent=res;
			})
		}
	})
})