<?php 
	require "connection.php";
	function validateform(){
		$brand = $_POST['brand'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category'];
		$image = $_FILES['image'];
		$file_types=["jpg", "jpeg", "png", "gif", "svg", "webp", "bitmap", "tiff", "tif"];
		$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

		$errors=0;
		if(!isset($brand) || $brand==""){
			$errors++;
		}
		if(!isset($price) || $price<0){
			$errors++;
		}
		if(!isset($description) || $description=""){
			$errors++;
		}
		if(!isset($category_id) || $category_id=""){
			$errors++;
		}
		if($_FILES['image']['size']>0 && !in_array($file_ext, $file_types)){
			$errors++;
		}
		if($errors>0){
			return false;
		}else{
			return true;
		}

	}

	if(validateform()){
		$id = $_POST['id'];
		$brand = $_POST['brand'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category'];

		$imgpath = "";
		$image_query="select imgpath from items where id=$id";
		$image=mysqli_fetch_assoc(mysqli_query($conn, $image_query));
		if($_FILES['image']['name']==""){
			$imgpath=$image['imgpath'];
		}else{
			$destination="../assets/images/";
			$file_name=$_FILES['image']['name'];
			move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);
			$imgpath=$destination.$file_name;
		}
		$update_query="update items set name='$brand', price=$price, description='$description', imgpath='$imgpath', category_id=$category_id where id=$id";
		$result=mysqli_query($conn, $update_query);
		header("Location: ../views/catalog.php");
	}else{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
 ?>