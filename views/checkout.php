<?php
	function get_title(){
		echo "Checkout";
	}
	require "../partials/template.php";

	function get_body_contents(){
		require "../controllers/connection.php";
?>
<h1 class="text-center py-3"></h1>
<hr>
<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<th>Item Name</th>
					<th>Item Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</thead>
				<tbody>
					<?php
						$total=0;
						if(isset($_SESSION['checkout'])){
							foreach ($_SESSION['checkout'] as $itemid => $quantity) {
								$item_query = "select*from items where id=$itemid";
								$indiv_item=mysqli_fetch_assoc(mysqli_query($conn, $item_query));
								$subtotal=$indiv_item['price']*$quantity;
								$total+=$subtotal;
					?>
					<tr>
						<td><?=$indiv_item['name']?></td>
						<td><?=$indiv_item['price']?></td>
						<td><span class="spanq"><?=$quantity?></span>
							<form action="../controllers/addtocheck-process.php" method="POST" class="d-none">
								<input type="hidden" name="id" value="<?= $itemid?>">
								<input type="hidden" name="fromcheckout" value="fromcheckout">
								<input type="number" name="checkout" class="form-control" value="<?=$quantity?>" data-id="<?= $itemid?>">
							</form>
						</td>
						<td><?=number_format($subtotal, 2)?></td>
						<td>
							<a href="../controllers/removeitem_process.php?id=<?= $itemid?>" class="btn btn-danger">Remove Item</a>
						</td>
					</tr>
					<?php
							}
						}
					?>
					<tr>
						<td></td>
						<td></td>
						<td>Total: </td>
						<td id="totalpayment"><?= number_format($total, 2, ".", ",")?></td>
						<td>
							<a href="../controllers/emptycheckout_process.php" class="btn btn-danger">Remove All</a>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<form action="../controllers/checkout-process.php" method="POST">
								<input type="hidden" name="totalpayment" value="<?= $total?>">
								<input type="hidden" name="cod" value="total">
								<button type="submit" class="btn btn-info">Pay via Cash</button>
							</form>
						</td>
						<td><div id="paypal-button-container"></div></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript" src="../assets/scripts/updatecart.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id=ARRXAltOfqBRQZWzo3ymY1KULwdcVqL_asM6WjaYkLl3Powb0gWz8lGZHkiAB2nZ2bquBakFBPfWK56c"></script>
<script>
	let totalpayment=document.getElementById('totalpayment').textContent.split(',').join("");
	console.log(totalpayment);

	paypal.Buttons({
	createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: totalpayment
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details) {
      	let data=new FormData;
      	data.append('totalpayment', totalpayment);
      	data.append('fromPayPal', 'fromPayPal');

      	fetch("../controllers/checkout-process.php", {
      		method:"POST",
      		body: data
      	}).then(res=>res.text())
      	.then(res=>{
      		console.log(res);
      		alert('Transaction completed by ' + details.payer.name.given_name);
      	})
      });
    }
}).render('#paypal-button-container');</script>
<?php
	}
?>