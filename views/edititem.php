<?php
	require "../partials/template.php";

	function get_title(){
		echo "Edit Item Form";
	}
	function get_body_contents(){
	require "../controllers/connection.php";
	$id=$_GET['id'];
	$item_query="select*from items where id=$id";
	$item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
?>
	<h1 class="text-center py-5">Edit Item Form</h1>
	<div class="container">
		<div class="col-lg-6 offset-lg-3 my-5">
			<form action="../controllers/edititem-process.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="brand">Brand Name:</label>
					<input type="text" name="brand" class="form-control" value="<?= $item['name']?>">
				</div>
				<div class="form-group">
					<label for="price">Price:</label>
					<input type="number" name="price" class="form-control" value="<?= $item['price']?>">
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea name="description" class="form-control"><?= $item['description']?></textarea>
				</div>
				<div class="form-group">
					<label for="image">Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category">Category</label>
					<select name="category" class="form-control">
						<?php

							$category_query="select*from categories";
							$category=mysqli_query($conn, $category_query);

							foreach ($category as $indiv_category) {
						?>
							<option value="<?= $indiv_category['id']?>"<?= $indiv_category['id'] == $item['category_id'] ? "selected":""?>><?= $indiv_category['name']?></option>
						<?php
							}
						?>
					</select>				
				</div>
				<input type="hidden" name="id" value="<?= $id ?>">
				<div class="text-center">
					<button type="submit" class="btn btn-success">Update Item</button>
				</div>
			</form>
		</div>
	</div>
<?php
	}
?>