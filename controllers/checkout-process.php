<?php

	require "connection.php";
	session_start();

	$userid=$_SESSION['user']['id'];
	$total=$_POST['totalpayment'];
	$payment_id=0;
	$status_id=0;

	if(isset($_POST['fromPayPal'])){
		$payment_id=2;
		$status_id=2;
	}

	if(isset($_POST['cod'])){
		$payment_id=1;
		$status_id=1;
	}

	$order_query="insert into orders(total, user_id, payment_id, status_id) values ($total, $userid, $payment_id, $status_id)";

	$order=mysqli_query($conn, $order_query);

	$order_id=mysqli_insert_id($conn);

	foreach ($_SESSION['checkout'] as $itemId=>$quantity) {
		$item_query="insert into item_order (item_id, order_id, quantity) values($itemId, $order_id, $quantity)";
		$result=mysqli_query($conn, $item_query);
	}

	unset($_SESSION['checkout']);
?>