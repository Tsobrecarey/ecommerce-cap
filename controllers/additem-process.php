<?php

	require "connection.php";
	
	$brand = $_POST['brand'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$category_id = $_POST['category'];

	$image = $_FILES['image'];
	$file_types=["jpg", "jpeg", "png", "gif", "svg", "webp", "bitmap", "tiff", "tif"];
	$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

	function validateform(){
		$brand = $_POST['brand'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category'];
		$image = $_FILES['image'];
		$file_types=["jpg", "jpeg", "png", "gif", "svg", "webp", "bitmap", "tiff", "tif"];
		$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

		$errors=0;
		if(!isset($brand) || $brand==""){
			$errors++;
		}
		if(!isset($price) || $price<0){
			$errors++;
		}
		if(!isset($description) || $description=""){
			$errors++;
		}
		if(!isset($category_id) || $category_id=""){
			$errors++;
		}
		if(!isset($image) || $image=""){
			$errors++;
		}
		if(!in_array($file_ext, $file_types)){
			$errors++;
		}
		if($errors>0){
			return false;
		}else{
			return true;
		}

	}

	if(validateform()){
		$destination="../assets/images/";
		$filename=$image['name'];
		$imgpath=$destination.$filename;
		move_uploaded_file($image['tmp_name'], $imgpath);

		$add_item_query = "insert into items(name, price, description, imgpath, category_id) values('$brand', $price, '$description', '$imgpath', '$category_id')";
		$new_item=mysqli_query($conn, $add_item_query);

		header("Location: ../views/catalog.php");
	}else{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
?>