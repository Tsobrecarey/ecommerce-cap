let spanqs=document.querySelectorAll(".spanq");

spanqs.forEach(indiv_spanq=>{
	indiv_spanq.parentElement.addEventListener("click", ()=>{
		indiv_spanq.nextElementSibling.classList.remove('d-none');
		indiv_spanq.classList.add('d-none');
	})

	let quantity=0;

	indiv_spanq.nextElementSibling.lastElementChild.addEventListener("change",()=>{
		quantity=indiv_spanq.nextElementSibling.lastElementChild.value;
		if(quantity<=0){
			alert("Invalid Quantity");
			indiv_spanq.classList.remove('d-none');
			indiv_spanq.nextElementSibling.classList.add('d-none');
		} else{
			indiv_spanq.nextElementSibling.submit();
		}
	})

	indiv_spanq.nextElementSibling.lastElementChild.addEventListener("keypress", (e)=>{
		console.log(quantity);
		if (e.keycode == 13 && quantity<=0){
			e.preventDefault();
			alert("Invalid Quantity");
			indiv_spanq.classList.remove('d-none');
			indiv_spanq.nextElementSibling.classList.add('d-none');
			indiv_spanq.nextElementSibling.lastElementChild.value=indiv_spanq.textContent;
		}
	})
		// let id=indiv_spanq.nextElementSibling.getAttribute('data-id');
		// let quantity=indiv_spanq.nextElementSibling.value;
		// let price=parseInt(indiv_spanq.parentElement.previousElementSibling.textContent)

		// indiv_spanq.classList.remove('d-none');
		// indiv_spanq.nextElementSibling.classList.add('d-none');

		// let data=new FormData;
		// data.append("id", id)
		// data.append("checkout", quantity);
		// data.append("fromcheckout", "fromcheckout");

		// fetch("../../controllers/addtocheck-process.php", {
		// 	method:"POST", body: data
		// }).then(res=> res.text())
		// .then(res=>{
		// 	indiv_spanq.classList.remove('d-none');
		// 	indiv_spanq.nextElementSibling.classList.add('d-none');
		// 	indiv_spanq.textContent=quantity;

		// 	indiv_spanq.parentElement.nextElementSibling.textContent=price*quantity;
		// 	document.getElementById('cartCount').textContent=res;

		// })
})