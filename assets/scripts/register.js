let firstname=document.getElementById("firstname");
let lastname=document.getElementById("lastname");
let email=document.getElementById("email");
let password=document.getElementById("password");
let confirmpassword=document.getElementById("confirmpassword");
let registerbtn=document.getElementById("registeruser");

const validate=(firstname, lastname, email, password, confirmpassword)=>{
	let errors=0;

	if(firstname.value==""){
		firstname.nextElementSibling.textContent="Please provide a valid First Name";
		errors++;
	}else{
		firstname.nextElementSibling.textContent="";
	}

	if(lastname.value==""){
		lastname.nextElementSibling.textContent="Please provide a valid Last Name";
		errors++;
	}else{
		lastname.nextElementSibling.textContent="";
	}

	if(email.value==""){
		email.nextElementSibling.textContent="Please provide a valid E-mail";
		errors++;
	}else{
		email.nextElementSibling.textContent="";
	}

	if(password.value==""){
		password.nextElementSibling.textContent="Please provide a valid Password";
		errors++;
	}else{
		password.nextElementSibling.textContent="";
	}

	if(confirmpassword.value==""){
		confirmpassword.nextElementSibling.textContent="Please provide a valid Password";
		errors++;
	}else{
		confirmpassword.nextElementSibling.textContent="";
	}

	if(password.value.length<8 || password.value.length>24){
		password.nextElementSibling.textContent="Password must be between 8-24 characters";
		errors++;
	}else{
		password.nextElementSibling.textContent="";
	}

	if (password.value!=confirmpassword.value){
		confirmpassword.nextElementSibling.textContent="Password does not match";
		errors++;
	}else{
		password.nextElementSibling.textContent="";
	}

	if(errors>0){
		return false;
	}else{
		return true;
	}
}

confirmpassword.addEventListener("input", ()=>{
	if (password.value!=confirmpassword.value){
		confirmpassword.nextElementSibling.textContent="Password does not match";
	}else{
		password.nextElementSibling.textContent="";
	}
});

registerbtn.addEventListener("click", ()=>{
	if(validate(firstname, lastname, email, password, confirmpassword)){
		let data=new FormData;
		data.append("firstname", firstname.value);
		data.append("lastname", lastname.value);
		data.append("email", email.value);
		data.append("password", password.value);

		fetch("../../controllers/register-process.php", {
			method: "POST",
			body:data
		})
		.then(res=>res.text())
		.then(res=>{
			if(res == "user_exists"){
				email.nextElementSibling.textContent="Email already registered!"
			}else{
				window.location.replace("login.php");
			}
		})
	}
})