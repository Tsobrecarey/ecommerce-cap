<?php
	require "../partials/template.php";

	function get_title(){
		echo "Catalog";
	}

	function get_body_contents(){
	require "../controllers/connection.php";
?>

	<h1 class="text-center py-5">Sound Life</h1>
	<div class="row">
		<?php 
			$items_query="select*from items";
			$items=mysqli_query($conn, $items_query);
			foreach ($items as $indiv_item) {
		?>
				<div class="col-lg-4 py-2">
					<div class="card">
						<img class="card-top" height=200px src="<?php echo $indiv_item['imgpath']?>">
						<div class="card-body">
							<h4 class="card-title"><?= $indiv_item['name']?></h4>
							<p class="card-text">Price: Php <?= $indiv_item['price']?></p>
							<p class="card-text">Description: <?= $indiv_item['description']?></p>
							<p class="card-text">Category: <?php

								$catid=$indiv_item['category_id'];
								$category_query ="select*from categories where id=$catid";
								$category=mysqli_fetch_assoc(mysqli_query($conn, $category_query));
							if ($category['id']=$catid){
								echo $category['name'];
							}


							?></p>
						</div>
						<div class="card-footer">
							<a href="../controllers/deleteitem-process.php?id=<?= $indiv_item['id'] ?>" class="btn btn-danger">Delete Item</a>
							<a href="edititem.php?id=<?= $indiv_item['id'] ?>" class="btn btn-info">Edit Item</a>
						</div>
						<div class="card-footer text-center">
							<input type="number" name="checkout" class="form-control" value="1">
							<button type="button" class="btn btn-primary checkbtn" data-id="<?= $indiv_item['id']?>">Add to Checkout</button>
						</div>
					</div>
				</div>

		<?php
			}
		?>
	</div>
	<script type="text/javascript" src="../assets/scripts/addtocheck.js"></script>
<?php } ?>