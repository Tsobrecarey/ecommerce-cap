<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="../index.php">Sound Life</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarColor01">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="catalog.php">Product List <span class="sr-only">(current)</span></a>
		      </li>
		      <?php
		      		session_start();
		      		if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
		      ?>
				    <li class="nav-item">
					    <a class="nav-link" href="additems.php">Add Item</a>
					</li>
		      <?php
		      		}else{
		      ?>
				    <li class="nav-item">
					    <a class="nav-link" href="checkout.php">Checkout<span class="badge bg-warning" id="cartCount">
		            <?php 
		                if(isset($_SESSION['checkout'])){
		                    echo array_sum($_SESSION['checkout']);
		                }else{
		                    echo 0;
		                }
		            ?></span></a></li>
		      <?php
		      }
		      		if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
		      ?>
				    <li class="nav-item">
					    <a class="nav-link" href="transaction.php">Transactions</a>
					</li>
		      <?php
		      		}else if(isset($_SESSION['user'])){
		      ?>
				    <li class="nav-item">
					    <a class="nav-link" href="history.php">History</a>
					</li>
		      <?php
		      }

		      if(isset($_SESSION['user'])){
		      ?>
		      		<li class="nav-item">
		      			<a href="../views/profile.php" class="nav-link">Hi <?= $_SESSION['user']['firstname']?>!</a>
		      		</li>
				  	<li class="nav-item">
				        <a class="nav-link" href="../controllers/logout-process.php">Logout</a>
				  	</li>
		      <?php
		      }else{
		      ?>
				    <li class="nav-item">
					    <a class="nav-link" href="login.php">Login</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="register.php">Register</a>
					</li>
		      <?php
		      }
		      ?>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="text" placeholder="Search">
		      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
		    </form>
	  	</div>
	</nav>

	<?php get_body_contents()?>

	<footer class="page-footer font-small">
		<div class="footer-copyright text-center py-3">© 2020 Made by Tim Sobrecarey</div>		
	</footer>
</body>
</html>