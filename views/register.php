<?php
	require "../partials/template.php";

	function get_title(){
		echo "Register";
	}

	function get_body_contents(){
?>
	<h1 class="text-center py-5">Register</h1>
	<div class="col-lg-8 offset-lg-2">
		<form action="" method="POST">
			<div class="form-group">
				<label for="firstname">First Name:</label>
				<input type="text" name="firstname" class="form-control" id="firstname">
				<span class="validation"></span>
			</div>
			<div>
				<label for="lastname">Last Name:</label>
				<input type="text" name="lastname" class="form-control" id="lastname">
				<span class="validation"></span>
			</div>
			<div>
				<label for="email">E-mail:</label>
				<input type="email" name="email" class="form-control" id="email">
				<span class="validation"></span>
			</div>
			<div>
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password">
				<span class="validation"></span>
			</div>
			<div>
				<label for="confirmpassword">Confirm Password:</label>
				<input type="password" name="confirmpassword" class="form-control" id="confirmpassword">
				<span class="validation"></span>
			</div>
			<button type="button" class="btn btn-info" id="registeruser">Register</button>
			<p>Already Registered? <a href="login.php">Login!</a></p>
		</form>
	</div>
	<script type="text/javascript" src="../assets/scripts/register.js"></script>
<?php
	}
?>