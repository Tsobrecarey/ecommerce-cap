<?php

	session_start();

	$id=$_POST['id'];
	$quantity=$_POST['checkout'];

	if(isset($_POST['fromcheckout'])){
		$_SESSION['checkout'][$id]=$quantity;
	}else{
		if(isset($_SESSION['checkout'][$id])){
			$_SESSION['checkout'][$id] += $quantity;
		}else{
			$_SESSION['checkout'][$id] = $quantity;
		}
	}

	function getchecksum(){
		return array_sum($_SESSION['checkout']);
	}
	echo getchecksum();

	header("Location: ".$_SERVER['HTTP_REFERER']);
?>