<?php
	require "../partials/template.php";

	function get_title(){
		echo "Transactions";
	}

	function get_body_contents(){
	require "../controllers/connection.php";
?>
	<h1 class="text-center py-3">Transactions</h1>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-striped">
					<thead>
						<th>Order ID</th>
						<th>User ID</th>
						<th>Item Name</th>
						<th>Total</th>
						<th>Payment</th>
						<th>Status</th>
					</thead>
					<tbody>
						<?php
							foreach ($_SESSION['transaction'] as $orderid) {
								$order_query="select*from orders where id=$orderid";
								$indiv_order=mysqli_fetch_assoc(mysqli_query($conn, $order_query))
						?>
								<td><?=$indiv_order['id']?></td>
								<td><?=$indiv_order['user_id']?></td>
								<td></td>
								<td></td>
								<td><?=$indiv_order['payment_id']?></td>
								<td><?=$indiv_order['status_id']?></td>
						<?php								
							}
						?>

					</tbody>					
				</table>
			</div>			
		</div>
	</div>
<?php
	}
?>