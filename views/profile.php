<?php
	require "../partials/template.php";

	function get_title(){
		echo "Register";
	}

	function get_body_contents(){
	require "../controllers/connection.php";
?>

	<h1 class="text-center py-5">Profile Page</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h1>Hello!</h1>
				<h3><?= $_SESSION['user']['firstname']?> <?= $_SESSION['user']['lastname']?></h3>
				<h4>Your registered e-mail is:</h4>
				<h3><?= $_SESSION['user']['email']?></h3>
			</div>
			<div class="col-lg-6">
				<h3>Addresses:</h3>
				<ul>
					<?php
						$userid = $_SESSION['user']['id'];
						$address_query = "select*from addresses where user_id=$userid";
						$addresses=mysqli_query($conn, $address_query);

						foreach ($addresses as $indiv_address) {
					?>
					<li><?= $indiv_address['address1'].", ".$indiv_address['address2']. "<br>".$indiv_address['city']. " ".$indiv_address['zipcode']?></li>
					<?php
						}
					?>
				</ul>
				<form action="../controllers/addaddress-process.php" method="POST">
					<div class="form-group">
						<label for="address1">Address 1:</label>
						<input type="text" name="address1" class="form-control">
					</div>
					<div class="form-group">
						<label for="address2">Address 2:</label>
						<input type="text" name="address2" class="form-control">
					</div>	
					<div class="form-group">
						<label for="city">City:</label>
						<input type="text" name="city" class="form-control">
					</div>	
					<div class="form-group">
						<label for="zipcode">Zip Code:</label>
						<input type="text" name="zipcode" class="form-control">
						<input type="hidden" name="user_id" value="<?= $userid?>">
					</div>
					<button class="btn btn-secondary">Add Address</button>
				</form>
			</div>
			<div class="col-lg-6 offset-lg-6">
				<h3>Contacts:</h3>
				<ul>
					<?php
						$userid = $_SESSION['user']['id'];
						$contact_query = "select*from contacts where user_id=$userid";
						$contacts=mysqli_query($conn, $contact_query);

						foreach ($contacts as $indiv_contact) {
					?>
					<li><?= $indiv_contact['contactno']?></li>
					<?php
						}
					?>
				</ul>
				<form action="../controllers/addcontact-process.php" method="POST">
					<div class="form-group">
						<label for="contact">Phone Number:</label>
						<input type="number" name="contact" class="form-control">
						<input type="hidden" name="user_id" value="<?= $userid?>">
					</div>
					<button class="btn btn-secondary">Add Number</button>
				</form>
			</div>
		</div>
	</div>

<?php
	}
?>